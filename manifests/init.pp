include apt

Exec['apt-update'] -> Package <||>

class php (
  $package_ensure = $php::params::package_ensure,
  $php_modules    = $php::params::modules
) inherits php::params {
    class { '::php::package':
      package_ensure => $package_ensure,
    }

    class { '::php::modules':
      package_ensure => $package_ensure,
      php_modules    => $php_modules
    }
}
