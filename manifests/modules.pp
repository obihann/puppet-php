class php::modules (
  $package_ensure = $php::params::package_ensure,
  $php_modules    = $php::params::php_modules
) inherits php::params {
    package { $php_modules:
        ensure  => $package_ensure,
        require => Package['php5'],
    }
}
