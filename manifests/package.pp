class php::package (
  $package_ensure = $php::params::package_ensure
) inherits php::params {
    package { 'php5':
        ensure => $package_ensure,
    }
}
