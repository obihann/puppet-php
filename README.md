# Puppet PHP
This is a basic puppet module that installs PHP 5.4 from the PPA ondrej/php5-oldstable.

## Usage
Base (php5, php5-mysql, php5-imagick, php5-mcrypt, php-pear, php5-curl)

```
include php
```

## Dependencies

- [puppetlabs/puppetlabs-apt](git@github.com:puppetlabs/puppetlabs-apt.git)
- [puppetlabs/puppetlabs-stdlib](https://github.com/puppetlabs/puppetlabs-stdlib)