class php::apache (
  $package_ensure = $php::params::package_ensure,
  $package_name   = $php::params::package_name,
) inherits php {
    package { 'libapache2-mod-php5':
        ensure  => $packge_ensure,
        require => Package[$package_name],
        notify  => Package['apache2']
    }
}
