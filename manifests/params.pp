class php::params {
  $package_ensure  = 'present'
  $php_modules     = [
      'php5-mysql',
      'php5-imagick',
      'php5-mcrypt',
      'php-pear',
      'php5-curl'
  ]
}
